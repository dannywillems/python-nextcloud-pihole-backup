# python-nextcloud-pihole-backup

Python script to backup pihole configurations in Nextcloud.

Environment variables:
- `HOSTNAME_NC`
- `USERNAME_NC`
- `PASSWORD_NC`
- `PIHOLE_BACKUP_DIR` (default `pihole_backup`)

## Install

Either use `poetry install` or, if `poetry` is slow on the machine:
```
pip install pyocclient python-decouple
```

## Run

```
python scripts/run.py
# or
poetry run python scripts/run.py
```
