import datetime
import decouple
import os
import owncloud

HOSTNAME_NC = decouple.config("HOSTNAME_NC")
USERNAME_NC = decouple.config("USERNAME_NC")
PASSWORD_NC = decouple.config("PASSWORD_NC")
PIHOLE_BACKUP_DIR = decouple.config("PIHOLE_BACKUP_DIR", default="pihole_backup")

OC = owncloud.Client(HOSTNAME_NC, verify_certs=False)
OC.login(USERNAME_NC, PASSWORD_NC)

LOCAL_FILENAME = "%s.tar.gz" % datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
REMOTE_FILENAME = "%s/%s" % (PIHOLE_BACKUP_DIR, LOCAL_FILENAME)

pihole_cmd = ["pihole", "-a", "-t", LOCAL_FILENAME]
os.popen(" ".join(pihole_cmd))

try:
    OC.mkdir(PIHOLE_BACKUP_DIR)
except owncloud.owncloud.HTTPResponseError:
    print("Supposing %s already exists" % PIHOLE_BACKUP_DIR)
OC.put_file(REMOTE_FILENAME, LOCAL_FILENAME)
